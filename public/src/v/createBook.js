pl.v.createBook = {
    setupUserInterface: function () {
        var saveButton = document.forms['Book'].commit;
        alert("createBook setup user interface!");
        //load all book objects
        Book.retrieveAll();
        //set an even handler for the save/submit button
        saveButton.addEventListener("click", pl.v.createBook.handleSaveButtonClickEvent);
        //handle the event when the brower window/tab is closed
        window.addEventListener("beforeunload", Book.saveAll);
    },
    handleSaveButtonClickEvent: function () {
        var formEl = document.forms['Book'];
        var slots = {isbn:formEl.isbn.value, title: formEl.title.value, year: formEl.year.value};
        Book.add( slots);
        alert("Added book!");
        formEl.reset();
    }
};
