function Book(slots) {
    this.isbn = slots.isbn;
    this.title = slots.title;
    this.year = slots.year;
};

Book.instances = {};

Book.convertRow2Obj = function (bookRow) {
    var book = new Book(bookRow);
    return book;
    alert("Book.convertRow2Obj");
};
Book.add = function (slots) {
    alert("Book.add");
    var book = new Book(slots);
    //add book to the collection of Book.instances
    Book.instances[slots.isbn] = book;
    console.log("Book " + slots.isbn + " created!");
};

Book.retrieveAll = function () {
    alert("Book.retrieveAll")
    var key="", keys=[], i=0, booksString="", books={};
    try {
        if (localStorage.getItem("books")) {
            booksString = localStorage.getItem("books");
        }
    } catch (e) {
        alert("Error when reading from Local Storage\n" + e);
    }
    if (booksString) {
        books = JSON.parse( booksString);
        keys = Object.keys( books);
        console.log( keys.length + " books loaded.");
        for (i = 0; i < keys.length; i++) {
            key = keys[i];
            Book.instances[key] = Book.convertRow2Obj( books[key]);
        }
    }
};

Book.update = function (slots) {
    alert("Book.update");
    var book = Book.instances[slots.isbn];
    var year = parseInt(slots.year);
    if (book.title !== slots.title) { book.title = slots.title;}
    if (book.year !== year) { book.year = year;}
    console.log("Book " + slots.isbn + " modified!");
};

Book.destroy = function (isbn) {
    alert("Book.destroy");
    if (Book.instances[isbn]) {
        console.log("Book " + isbn + " deleted");
        delete Book.instances[isbn];
    } else {
        console.log("There is no book with ISBN " + isbn + " in the database.");
    }
};

Book.saveAll = function () {
    alert("Book.saveAll");
    var booksString = "", error=false, nmrOfBooks = Object.keys(Book.instances).length;
    try {
        booksString = JSON.stringify(Book.instances);
        localStorage.setItem("books",booksString);
    } catch (e) {
        alert("Error when writing to local storage\n" + e);
        error = true;
    }
    if (!error) console.log(nmrOfBooks + " books saved.");
};

Book.createTestData = function () {
    alert("Book.createTestData");
    Book.instances["006251587X"] = new Book({isbn:"006251587X", title:"Weaving the Web", year:2000});
    Book.instances["0465026567"] = new Book({isbn:"0465026567", title:"Godel, Escher, Bach", year:1999});
    Book.instances["0465030793"] = new Book({isbn:"0465030793", title:"I am a strange loop", year:2008});
    Book.saveAll();
};

Book.clearData = function () {
    if (confirm("Do you really want to delete all book data?")){
        Book.instances = {};
        localStorage.setItem("books", "{}");
    }
    alert("Book.clearData");
};
